package logic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;

import java.util.List;

import audio.MyMediaPlayer;
import audio.MySoundPool;
import common.CommonVariables;
import data.Data;
import heartlesskokoro.puzzleimage.R;

public class PuzzleFragment extends Fragment {

    public PuzzleSurface puzzleSurface;
    public MyMediaPlayer myMediaPlayer;
    public MySoundPool mySoundPool;
    public CommonVariables common = CommonVariables.getInstance();
    public SharedPreferences sharedpreferences;
    public Menu menu;
    public NoisyAudioStreamReceiver noisyAudioStreamReceiver;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    private static final String TAG = "puzzleLog";

    // start of receiver inner class to handle headphones becoming unplugged
    public class NoisyAudioStreamReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (common.isLogging)
                Log.d(TAG, "onReceive NoisyAudioStreamReceiver");

            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent
                    .getAction())) {
                // quiet the media player
                if (myMediaPlayer != null) {
                    myMediaPlayer.setNewVolume(0.1f);
                }
            }
        }
    }

    private IntentFilter intentFilter = new IntentFilter(
            AudioManager.ACTION_AUDIO_BECOMING_NOISY);

    private void startPlayback() {
        if (common.isLogging)
            Log.d(TAG, "startPlayback NoisyAudioStreamReceiver");
        getActivity().registerReceiver(noisyAudioStreamReceiver, intentFilter);
    }

    private void stopPlayback() {
        if (common.isLogging)
            Log.d(TAG, "stopPlayback NoisyAudioStreamReceiver");

        getActivity().unregisterReceiver(noisyAudioStreamReceiver);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (common.isLogging)
            Log.d(TAG, "onCreate PuzzleFragment");

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                if (result.getPostId() != null)
                    common.showToast("facebook post success");
            }

            @Override
            public void onCancel() {
                common.showToast("facebook post canceled");
            }

            @Override
            public void onError(FacebookException e) {
                common.showToast("facebook post error");
            }
        });
        common.shareDialog = shareDialog;
    }

    @Override
    public void onStart() {
        if (common.isLogging)
            Log.d(TAG, "onStart PuzzleFragment");

        super.onStart();
        getSharedPrefs();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        if (common.isLogging)
            Log.d(TAG, "onActivityCreated PuzzleFragment");

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (common.isLogging)
            Log.d(TAG, "onActivityResult PuzzleFragment");

        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getSharedPrefs() {
        if (common.isLogging)
            Log.d(TAG, "getSharedPrefs PuzzleFragment");

        sharedpreferences = getActivity().getSharedPreferences(
                getString(R.string.MY_PREFERENCES), Context.MODE_PRIVATE);

        // check for all to be loaded here
        boolean isValid = false;

        int posImage = 0;

        if (sharedpreferences.contains(getString(R.string.COLUMN_IMAGENUMBER))) {
            posImage = sharedpreferences.getInt(
                    getString(R.string.COLUMN_IMAGENUMBER), 0);
            if (posImage >= 0 || posImage < Data.PICS.length) {
                isValid = true;
            }
        } else {
            isValid = false;
        }

        // only continue if there is an image from the previous puzzle
        String slots = "";
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_SLOTS))) {
                slots = sharedpreferences.getString(
                        getString(R.string.COLUMN_SLOTS), "");
                if (slots == null || slots.equals("") || slots.length() < 2) {
                    isValid = false;
                } else {
                    String[] slotArr = slots.split(",");
                    int expectedTotal = sumToPositiveN(slotArr.length - 1);
                    int actualTotal = 0;
                    for (String aSlotArr : slotArr) {
                        try {
                            int temp = Integer.parseInt(aSlotArr);
                            actualTotal += temp;
                        } catch (NumberFormatException ignored) {
                        }
                    }
                    common.dimensions = Math.sqrt((double) slotArr.length);
                    if (expectedTotal != actualTotal) isValid = false;
                }
            } else isValid = false;
        }

        boolean playTap = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_SOUND))) {
                playTap = sharedpreferences.getBoolean(
                        getString(R.string.COLUMN_SOUND), true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean playChime = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_CHIME))) {
                playChime = sharedpreferences.getBoolean(
                        getString(R.string.COLUMN_CHIME), true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean playMusic = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_MUSIC))) {
                playMusic = sharedpreferences.getBoolean(
                        getString(R.string.COLUMN_MUSIC), true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        boolean drawBorders = false;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_BORDER))) {
                drawBorders = sharedpreferences.getBoolean(
                        getString(R.string.COLUMN_BORDER), true);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        int posSound = 0;
        if (isValid) {
            if (sharedpreferences.contains(getString(R.string.COLUMN_POSITION))) {
                posSound = sharedpreferences.getInt(
                        getString(R.string.COLUMN_POSITION), 0);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        Long currentTime = 0l;
        if (isValid) {
            if (sharedpreferences.contains("TIME")) {
                currentTime = sharedpreferences.getLong("TIME", 0l);
                isValid = true;
            } else {
                isValid = false;
            }
        }

        if (common.isLogging)
            Log.d(TAG, "getSharedPrefs PuzzleFragment valid: " + isValid);

        if (isValid) {
            common.currentPuzzleImagePosition = posImage;
            common.currentSoundPosition = posSound;
            common.drawBorders = drawBorders;
            common.playMusic = playMusic;
            common.playChimeSound = playChime;
            common.playTapSound = playTap;
            common.setSlots(slots);
            common.resumePreviousPuzzle = true;
            common.currPuzzleTime = currentTime;
        } else {
            common.createNewPuzzle = true;
        }
    }

    private int sumToPositiveN(int n) {
        if (n <= 0)
            return 0;
        return sumToPositiveN(n - 1) + n;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (common.isLogging)
            Log.d(TAG, "onCreateOptionsMenu PuzzleFragment");

        inflater.inflate(R.menu.main_puzzle, menu);
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {
        if (common.isLogging)
            Log.d(TAG, "onOptionsMenuClosed PuzzleFragment");

        super.onOptionsMenuClosed(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (common.isLogging)
            Log.d(TAG, "onOptionsItemSelected PuzzleFragment");

        switch (item.getItemId()) {
            case R.id.new_puzzle:
                puzzleSurface.newPuzzle();
                return true;
            case R.id.music_toggle:
                puzzleSurface.toggleMusic();
                return true;
            case R.id.set_toggle:
                puzzleSurface.toggleSetSound();
                return true;
            case R.id.win_toggle:
                puzzleSurface.toggleWinSound();
                return true;
            case R.id.border_toggle:
                puzzleSurface.toggleBorder();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void audioInit() {
        if (common.isLogging)
            Log.d(TAG, "audioInit PuzzleFragment");

        myMediaPlayer = new MyMediaPlayer();
        myMediaPlayer.init();
        puzzleSurface.myMediaPlayer = myMediaPlayer;
        mySoundPool = new MySoundPool(15, AudioManager.STREAM_MUSIC, 100);
        mySoundPool.init();
        common.mySoundPool = mySoundPool;
    }

    private void referenceUIComponents(View view) {
        // The UI has a puzzle
        if (common.isLogging)
            Log.d(TAG, "referenceUIComponents PuzzleFragment");

        puzzleSurface = (PuzzleSurface) view.findViewById(R.id.puzzle);

        common.mNextButton = ((Button) view.findViewById(R.id.nextButton));
        common.mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.nextImage();
            }
        });

        common.facebookLinkButton = ((ImageButton) view
                .findViewById(R.id.facebookButton));

        common.facebookLinkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.facebookActivity();
            }
        });

        common.wordpressLinkButton = ((ImageButton) view
                .findViewById(R.id.wordpressButton));
        common.wordpressLinkButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                puzzleSurface.wordpressActivity();
            }
        });

        common.shareButton = (ShareButton) view.findViewById(R.id.fb_share_button);
        common.shareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                common.shareButton.setVisibility(View.INVISIBLE);
                facebookActivity();
            }
        });
    }

    public void facebookActivity() {
        common.hideButtons();
        common.showToast("Accessing Facebook App...");
        boolean hasFacebook = false;

        final PackageManager pm = common.context.getPackageManager();
        List<ApplicationInfo> packages = pm
                .getInstalledApplications(PackageManager.GET_META_DATA);
        for (ApplicationInfo packageInfo : packages) {
            Log.d(TAG, "Installed package : " + packageInfo.packageName);
            String name = packageInfo.packageName;
            if (name.equals("com.facebook.katana")) {
                hasFacebook = true;
            }
        }

        if (hasFacebook) {
            //share image to fb put into button functionality
            SharePhoto photo = new SharePhoto.Builder()
                    .setBitmap(common.image)
                    .build();
            SharePhotoContent content = new SharePhotoContent.Builder()
                    .addPhoto(photo)
                    .build();
            common.shareDialog.show(this, content);
        } else {
            common.showToast("Install Facebook App!");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (common.isLogging)
            Log.d(TAG, "onCreateView PuzzleFragment");

        final View view = inflater.inflate(R.layout.puzzle_layout, container,
                false);
        referenceUIComponents(view);
        audioInit();

        return view;
    }

    @Override
    public void onResume() {
        if (common.isLogging)
            Log.d(TAG, "onResume PuzzleFragment");

        super.onResume();
        AudioManager audioManager = (AudioManager) getActivity()
                .getSystemService(Context.AUDIO_SERVICE);

        float streamVolume = (float) audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC);

        common.volume = streamVolume
                / (float) audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC);

        noisyAudioStreamReceiver = new NoisyAudioStreamReceiver();
        startPlayback();

        if (common.playMusic) {
            myMediaPlayer.resume();
        } else {
            myMediaPlayer.abandonFocus();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (common.isLogging)
            Log.d(TAG, "onPause PuzzleFragment");

        stopPlayback();

        if (myMediaPlayer != null)
            myMediaPlayer.pause();

        if (puzzleSurface != null)
            puzzleSurface.onPause();
    }

    @Override
    public void onStop() {
        if (common.isLogging)
            Log.d(TAG, "onStop PuzzleFragment");

        super.onStop();
        myMediaPlayer.onStop();
        String slotString = puzzleSurface.getSlotString();
        Long dateLong = common.currPuzzleTime;

        Editor editor = sharedpreferences.edit();
        editor.putInt(getString(R.string.COLUMN_IMAGENUMBER),
                common.currentPuzzleImagePosition);
        editor.putString(getString(R.string.COLUMN_SLOTS), slotString);
        editor.putBoolean(getString(R.string.COLUMN_SOUND), common.playTapSound);
        editor.putBoolean(getString(R.string.COLUMN_MUSIC), common.playMusic);
        editor.putBoolean(getString(R.string.COLUMN_CHIME), common.playChimeSound);
        editor.putBoolean(getString(R.string.COLUMN_BORDER), common.drawBorders);
        editor.putInt(getString(R.string.COLUMN_POSITION),
                common.currentSoundPosition);
        editor.putLong("TIME", dateLong);
        editor.apply();
    }

    @Override
    public void onDestroy() {
        if (common.isLogging)
            Log.d(TAG, "onDestroy PuzzleFragment");

        super.onDestroy();
        if (mySoundPool != null) {
            mySoundPool.release();
            mySoundPool = null;
        }

        if (myMediaPlayer != null) {
            myMediaPlayer.cleanUp();
            myMediaPlayer = null;
        }

        if (puzzleSurface != null) {
            puzzleSurface.cleanUp();
            puzzleSurface = null;
        }
    }
}